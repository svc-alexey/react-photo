import React from 'react';
import {createUseStyles} from 'react-jss';

const ColoredUserPics = ({src, size, margin, colorWidth, bgk, colors}) => {
    console.log(size + margin + colorWidth)
    const useStyles = createUseStyles({
        wrapper: {
            display: "flex",
            width: '100%',
            height: '100vh',
            alignItems: 'center',
            justifyContent: 'center',
        },
        container: {
            display: "flex",
            alignItems: 'center',
            justifyContent: 'center',
            backgroundImage: `linear-gradient(to bottom,  ${colors[0]} 0%,${colors[1]} 100%)`,
            width: `${size + margin + colorWidth*2}px`,
            height: `${size + margin + colorWidth*2}px`,
            borderRadius: '50%',
            "&:hover": {
                background: `linear-gradient(to left,  ${colors[1]} 0%,${colors[0]} 100%)`,
            },
        },
        img: {
            backgroundImage: `url(${src})`,
            backgroundSize: 'cover',
            width: `${size}px`,
            height: `${size}px`,
            borderRadius: '50%',
            border: `${margin}px solid ${bgk}`
        },
    });

    const classes = useStyles();
    return (
        <div className={classes.wrapper}>
            <div className={classes.container}>
                <div className={classes.img}></div>
            </div>
        </div>
    )
}

export default ColoredUserPics;