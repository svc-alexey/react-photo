import './App.css';
import ColoredUserPics from "./ColoredUserPic";
import photo from './img/photo.jpg';

function App() {
  return (
    <div className="app">
      <ColoredUserPics
          src={photo}
          size={150}
          margin={4}
          colorWidth={6}
          bgk={'#ff0000'}
          colors={['#009933', '#0066ff']}
      />
    </div>
  );
}

export default App;
